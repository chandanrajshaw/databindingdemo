package kashyap.chandan.databindingpractice2021;

import android.util.Log;

import javax.inject.Inject;

public class User {
    String phone,pass;
    @Inject
    public User() {
        Log.i("LoginDagger","User instanceCreated");
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {

        this.pass = pass;
    }

}

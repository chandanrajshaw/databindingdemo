package kashyap.chandan.databindingpractice2021;

import kashyap.chandan.databindingpractice2021.Responses.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterface {
    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("loginservices/login")
    Call<LoginResponse> login(@Field("email_phone") String email_phone,
                              @Field("password") String password);
}

package kashyap.chandan.databindingpractice2021.Repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import kashyap.chandan.databindingpractice2021.ApiClientComponent;
import kashyap.chandan.databindingpractice2021.ApiInterface;
import kashyap.chandan.databindingpractice2021.DaggerApiClientComponent;
import kashyap.chandan.databindingpractice2021.MainActivity;
import kashyap.chandan.databindingpractice2021.Responses.LoginData;
import kashyap.chandan.databindingpractice2021.Responses.LoginResponse;
import kashyap.chandan.databindingpractice2021.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginRepository {
    @Inject
    Retrofit retrofit;
ApiClientComponent apiClientComponent;

MutableLiveData<LoginData> mutableLiveData=new MutableLiveData<>();
    SharedPreferences sharedPreferences;
    @Inject
    public LoginRepository() {
        apiClientComponent= DaggerApiClientComponent.create();
        apiClientComponent.inject(LoginRepository.this);
    }
  public   MutableLiveData<LoginData> getLoginStatus(User user, MainActivity activity)
    {

        ApiInterface apiInterface=retrofit.create(ApiInterface.class);
        Call<LoginResponse>call=apiInterface.login(user.getPhone(),user.getPass());
        HandlerThread handlerThread=new HandlerThread("Login_Handler");
        handlerThread.start();
        Handler handler=new Handler(handlerThread.getLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if (response.code()==200)
                        {
                            mutableLiveData.postValue(response.body().getData());
                            sharedPreferences=activity.getSharedPreferences("Login",Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor=sharedPreferences.edit();
                            editor.putString("username",response.body().getData().getEmail());
                            editor.putString("id",response.body().getData().getId());
                            editor.commit();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(activity,""+t.getMessage(),Toast.LENGTH_SHORT);

                            }
                        });
                    }
                });
            }
        });

        return mutableLiveData;
    }

}

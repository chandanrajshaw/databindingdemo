package kashyap.chandan.databindingpractice2021;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.Observable;
import androidx.databinding.ObservableField;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.telecom.Connection;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import kashyap.chandan.databindingpractice2021.Responses.LoginData;
import kashyap.chandan.databindingpractice2021.ViewModel.LoginViewModel;
import kashyap.chandan.databindingpractice2021.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
ActivityMainBinding mainBinding;
private LoginViewModel viewModel;
ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(MainActivity.this).get(LoginViewModel.class);
        connectionDetector = new ConnectionDetector(this);
//       mainBinding.executePendingBindings();
        mainBinding.getUser();
        mainBinding.progressCircular.setVisibility(View.GONE);
//       mainBinding.btnLogin.setOnClickListener(new View.OnClickListener() {
//           @Override
//           public void onClick(View view) {
//               btnLogin();
//           }
//       });
//    }
    }
   public void btnLogin(User user)
   {
       String username=mainBinding.etPhone.getText().toString();
       String pwd=mainBinding.etPassword.getText().toString();
Log.d("MSG","onClick");
if(username.isEmpty()&&pwd.isEmpty())
{
    Toast.makeText(MainActivity.this, "Enter all Fields", Toast.LENGTH_SHORT).show();
    Log.d("MSG","Enter All Fields");
}
else if (username.isEmpty()||username.length()!=10)
{
    Toast.makeText(MainActivity.this, "Enter Valid Phone", Toast.LENGTH_SHORT).show();
}
else if (pwd.isEmpty()||pwd.length()<6)
{
    Toast.makeText(MainActivity.this, "Enter Valid Password", Toast.LENGTH_SHORT).show();
}
else
{
    if (connectionDetector.isConnectingToInternet())
    {
//        User user=new User();
        user.setPass(pwd);
        user.setPhone(username);
        mainBinding.progressCircular.setVisibility(View.VISIBLE);
        viewModel.getLoginData(user,MainActivity.this).observe(MainActivity.this, new Observer<LoginData>() {
            @Override
            public void onChanged(LoginData loginData) {
                if (loginData!=null)
                {
                    Toast.makeText(MainActivity.this, "Login Successfull", Toast.LENGTH_SHORT).show();
                    mainBinding.progressCircular.setVisibility(View.GONE);
                }
                else
                {
                    Toast.makeText(MainActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                    mainBinding.progressCircular.setVisibility(View.GONE);
                }
            }
        });
    }
    else
    {
        Toast.makeText(this, "Plzz Connect to Internet First", Toast.LENGTH_SHORT).show();
    }
}
   }
}
package kashyap.chandan.databindingpractice2021.Responses;

import com.google.gson.annotations.SerializedName;

public class LoginData
{

	@SerializedName("image")
	private String image;

	@SerializedName("role")
	private String role;

	@SerializedName("registration_datetime")
	private String registrationDatetime;

	@SerializedName("phone")
	private String phone;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("id")
	private String id;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("email")
	private String email;

	public String getImage(){
		return image;
	}

	public String getRole(){
		return role;
	}

	public String getRegistrationDatetime(){
		return registrationDatetime;
	}

	public String getPhone(){
		return phone;
	}

	public String getLastName(){
		return lastName;
	}

	public String getId(){
		return id;
	}

	public String getFirstName(){
		return firstName;
	}

	public String getEmail(){
		return email;
	}
}
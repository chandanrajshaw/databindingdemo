package kashyap.chandan.databindingpractice2021.Responses;

import com.google.gson.annotations.SerializedName;

public class LoginResponse{

	@SerializedName("data")
	LoginData loginData;

	@SerializedName("status")
	private RedeemStatus status;

	public LoginData getData(){
		return loginData;
	}

	public RedeemStatus getStatus(){
		return status;
	}
}
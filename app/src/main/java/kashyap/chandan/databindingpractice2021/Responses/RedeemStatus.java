package kashyap.chandan.databindingpractice2021.Responses;

import com.google.gson.annotations.SerializedName;

public class RedeemStatus {

	@SerializedName("code")
	private int code;

	@SerializedName("message")
	private String message;

	public int getCode(){
		return code;
	}

	public String getMessage(){
		return message;
	}
}
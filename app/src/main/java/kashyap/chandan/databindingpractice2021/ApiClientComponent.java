package kashyap.chandan.databindingpractice2021;

import javax.inject.Singleton;

import dagger.Component;
import kashyap.chandan.databindingpractice2021.Repository.LoginRepository;
@Singleton
@Component(modules = {ApiClientModule.class})
public interface ApiClientComponent {
    public void inject(LoginRepository loginRepo);
    LoginRepository getLoginRepository();
}

package kashyap.chandan.databindingpractice2021;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiClientModule {
    public static final String IMAGE_URL = "https://ahnion.com/app/";
    public static final String BASE_URL1 = "https://ahnion.com/app/services/";
    public ApiClientModule(){}
    @Singleton
    @Provides
    public Retrofit provideRetrofit()
            {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient client = new OkHttpClient.Builder().connectTimeout(5, TimeUnit.MINUTES)
                        .readTimeout(5, TimeUnit.MINUTES)
                        .writeTimeout(5, TimeUnit.MINUTES).addInterceptor(interceptor).build();
                Gson gson = new GsonBuilder()
                        .setLenient()
                        .create();
                return new Retrofit.Builder().baseUrl(BASE_URL1).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
            }
}

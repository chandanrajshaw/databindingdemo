package kashyap.chandan.databindingpractice2021.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import javax.inject.Inject;

import kashyap.chandan.databindingpractice2021.ApiClientComponent;
import kashyap.chandan.databindingpractice2021.ApiClientModule;
import kashyap.chandan.databindingpractice2021.DaggerApiClientComponent;
import kashyap.chandan.databindingpractice2021.MainActivity;
import kashyap.chandan.databindingpractice2021.Repository.LoginRepository;
import kashyap.chandan.databindingpractice2021.Responses.LoginData;
import kashyap.chandan.databindingpractice2021.User;

public class LoginViewModel extends AndroidViewModel {

    @Inject
    LoginRepository loginRepository;
    ApiClientComponent apiClientComponent;
    public LoginViewModel(@NonNull Application application) {
        super(application);
        apiClientComponent= DaggerApiClientComponent.builder().apiClientModule(new ApiClientModule()).build();
    }
public LiveData<LoginData> getLoginData(User user, MainActivity mainActivity)
{
    loginRepository=apiClientComponent.getLoginRepository();
    return loginRepository.getLoginStatus(user,mainActivity);
}
}
